const path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

var config = {
  entry: "./src/main.js",
  output: {
    path: "./build",
    filename: "bundle.js"
  },

  devServer: {
    host: '0.0.0.0',
    port: 8080,
    contentBase: path.join(__dirname, './build')
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /(node_modules|bower_components)/,
        query: {
          presets: ['es2015', 'react'],
          cacheDirectory: true
        }
      },
      {
        test: /\.less$/,
        // loader: ExtractTextPlugin.extract('style-loader', 'css!postcss-loader!less'),
        loader: 'style!css!postcss-loader!less',
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.(png|jpg|svg|gif)/,
        loader: 'file?name=images/[name].[ext]'
      },
      {
        test: /\.(ttf|eof|woff|woff2)/,
        loader: 'file?name=fonts/[name].[ext]'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].css')
  ],
  postcss: function() {
    return [autoprefixer];
  }
};


module.exports = config;
