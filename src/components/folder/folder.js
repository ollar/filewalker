import React from 'react';
import {connect} from 'react-redux';
import './styles.less';

import {enterFolder} from '../../redux/actions/fileWalkerActions';

class Folder extends React.Component {
  constructor() {
    super();
    this.state = {
      isSelected: false
    };
  }

  selectToggle() {
    this.setState({isSelected: !this.state.isSelected});
  }

  enterFolder(folderName) {
    if (this.props.type === 'folder') {
      this.props.dispatch(enterFolder(folderName));
    }
  }

  render() {
    let _className = `leaf ${this.props.type} ${(this.state.isSelected ? ' selected' : '')}`;
    return (
      <div className={_className} onClick={ this.selectToggle.bind(this) } onDoubleClick={ this.enterFolder.bind(this, this.props.children) }>
        { this.props.children }
      </div>
    );
  }
}

function props(state) {
  return {
    folder: state.folder
  };
}

export default connect(props)(Folder);
