import React from 'react';
import {connect} from 'react-redux';

import FolderTrace from '../trace/trace';
import Folder from '../folder/folder';

class FileWalker extends React.Component {
  getCurrentTree(structure = this.props.fileWalker.structure, trace = this.props.fileWalker.folderTrace) {
    if (trace.length) {
      let key = trace[0];
      let ind = structure.findIndex((item) => {
        return Object.keys(item)[0] === key;
      });

      return this.getCurrentTree(structure[ind][key], trace.slice(1, trace.length));
    }

    return structure;
  }

  render() {
    let _structure = this.getCurrentTree().map((folder, i) => {
      let type = (typeof folder === 'object' ? 'folder' : 'file');
      let item = (type === 'folder' ? Object.keys(folder) : folder);
      return (<Folder key={ i } type={ type }>{ item }</Folder>);
    });
    return (
      <div className="file-walker">
        <FolderTrace />
        <div>
          {_structure}
        </div>
      </div>
    );
  }
}

function props(state) {
  return {
    fileWalker: state.fileWalker
  };
}

export default connect(props)(FileWalker);
