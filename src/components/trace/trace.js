import React from 'react';
import {connect} from 'react-redux';
import {updateFolderTrace} from '../../redux/actions/fileWalkerActions';
import './styles.less';

class FolderTrace extends React.Component {
  handleClick(i) {
    this.props.dispatch(updateFolderTrace(this.props.fileWalker.folderTrace.slice(0,i)));
  }

  genTrace() {
    return this.props.fileWalker.folderTrace.map((item, i) => {
      return (
        <div key={ i }>
          <div className="name" onClick={ this.handleClick.bind(this, i) }>{ item }</div>
          { (this.props.fileWalker.folderTrace.length - 1 > i) ? <div className="arrow" /> : '' }
        </div>
      );
    });
  }

  render() {
    return (
      <div className="folder-trace">{ this.genTrace() }</div>
    )
  }
}

function props(state) {
  return {
    fileWalker: state.fileWalker
  };
}

export default connect(props)(FolderTrace);
