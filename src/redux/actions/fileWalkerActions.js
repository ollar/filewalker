export function enterFolder(folderName) {
  return {
    type: 'FILEWALKER_ENTER_FOLDER',
    folderName: folderName
  };
}

export function updateFolderTrace(folders) {
  return {
    type: 'FILEWALKER_UPDATE_FOLDERTRACE',
    folders: folders
  };
}
