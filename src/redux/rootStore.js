import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from './rootReducer';

import createLogger from 'redux-logger';

const loggerMiddleware = createLogger();

const createStoreWithMiddlewares = compose(
  applyMiddleware(loggerMiddleware),
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
)(createStore);

const configureStore = (initialState) => {
  return createStoreWithMiddlewares(rootReducer, initialState);
};

const store = configureStore();

export default store;
