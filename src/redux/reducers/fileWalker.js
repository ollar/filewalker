const initialState = {
  structure: [
    {Home: [
      {Videos: [
        '1.avi',
        '2.avi'
      ]},
      {Music: [
        {Metallica: [
          '1.mp3',
          '2.mp3'
        ]},
        {ACDC: [
          '1.mp3',
          '2.mp3'
        ]},
        {Noname: [
          '1.mp3',
          '2.mp3'
        ]},
        '1.mp3',
        '2.mp3'
      ]},
      {Documents: [
        '1.doc',
        '2.doc'
      ]}
    ]}
  ],
  folderTrace: []
}

export default function fileWalkerReducer(state = initialState, action) {
  switch (action.type) {
    case 'FILEWALKER_ENTER_FOLDER':
      return Object.assign({}, state, {folderTrace: [].concat([...state.folderTrace], action.folderName)});

    case 'FILEWALKER_UPDATE_FOLDERTRACE':
      return Object.assign({}, state, {folderTrace: action.folders});

    default:
      return state;
  }
}
