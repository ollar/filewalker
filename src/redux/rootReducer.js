import {combineReducers} from 'redux';

import fileWalkerReducer from './reducers/fileWalker';

const rootReducer = combineReducers({
  fileWalker: fileWalkerReducer
});

export default rootReducer;
