import React from 'react';
import ReactDOM from 'react-dom';
import store from './redux/rootStore';
import './main.less';

import FileWalker from './components/fileWalker/fileWalker';

import {Provider} from 'react-redux';

ReactDOM.render(
  <Provider store={store}>
    <FileWalker />
  </Provider>,
  document.getElementById('application')
);
